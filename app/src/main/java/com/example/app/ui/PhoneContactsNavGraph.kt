package com.example.app.ui

import androidx.compose.material.DrawerState
import androidx.compose.material.DrawerValue
import androidx.compose.material.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.app.di.AppContainer
import com.example.app.presentation.*
import com.example.app.ui.PhoneContactsDestinationsArgs.PHONE_CONTACT_ID_ARG
import com.example.app.ui.addEditPhoneContact.AddEditPhoneContactScreen
import com.example.app.ui.phoneContact.PhoneContactScreen
import com.example.app.ui.phoneContacts.PhoneContactsRoute
import com.example.app.ui.searchPhoneContacts.SearchPhoneContactsScreen
import com.example.app.ui.searchPhoneContacts.SearchPhoneContactsScreenAttrs
import com.example.app.ui.statistics.StatisticsScreen
import com.example.app.ui.util.AppModalDrawer
import com.example.app.ui.util.getPhoneContactsViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
@Suppress("FunctionNaming", "LongParameterList", "LongMethod")
fun PhoneContactsNavGraph(
    appContainer: AppContainer,
    modifier: Modifier = Modifier,
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    drawerState: DrawerState = rememberDrawerState(initialValue = DrawerValue.Closed),
    navController: NavHostController = rememberNavController(),
    startDestination: String = PhoneContactsDestinations.PHONE_CONTACTS_ROUTE,
    navActions: PhoneContactsNavigationActions = remember(navController) {
        PhoneContactsNavigationActions(navController)
    }
) {
    val currentNavBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = currentNavBackStackEntry?.destination?.route ?: startDestination

    NavHost(
        navController = navController,
        startDestination = startDestination,
        modifier = modifier
    ) {
        composable(PhoneContactsDestinations.PHONE_CONTACTS_ROUTE) {
            val viewModel: PhoneContactsViewModel =
                viewModel(factory = getPhoneContactsViewModelFactory(appContainer = appContainer))
            AppModalDrawer(drawerState, currentRoute, navActions) {
                PhoneContactsRoute(
                    viewModel = viewModel,
                    startSearch = { navActions.navigateToSearchPhoneContacts() },
                    onAddItem = { navActions.navigateToAddEditPhoneContact(null) },
                    onItemClick = { itemId -> navActions.navigateToPhoneContact(itemId) },
                    openDrawer = { coroutineScope.launch { drawerState.open() } }
                )
            }
        }
        composable(PhoneContactsDestinations.SEARCH_PHONE_CONTACTS_ROUTE) {
            val viewModel: SearchPhoneContactsViewModel =
                viewModel(factory = getPhoneContactsViewModelFactory(appContainer = appContainer))
            val screenAttrs = SearchPhoneContactsScreenAttrs(
                viewModel = viewModel,
                onItemClick = { itemId -> navActions.navigateToPhoneContact(itemId) },
                onBack = { navController.popBackStack() }
            )
            SearchPhoneContactsScreen(
                screenAttrs = screenAttrs
            )
        }
        composable(
            PhoneContactsDestinations.PHONE_CONTACT_ROUTE,
            arguments = listOf(
                navArgument(PHONE_CONTACT_ID_ARG) {
                    type = NavType.IntType
                }
            )
        ) { entry ->
            val viewModel: PhoneContactViewModel =
                viewModel(
                    factory = getPhoneContactsViewModelFactory(
                        appContainer = appContainer,
                        entry.arguments
                    )
                )
            val screenAttributes = PhoneContactScreenAttributes(
                onEditItem = { navActions.navigateToAddEditPhoneContact(it) },
                onDeleteItem = { navActions.navigateToPhoneContacts() },
                onBack = { navController.popBackStack() }
            )

            PhoneContactScreen(
                viewModel = viewModel,
                screenAttrs = screenAttributes
            )
        }
        composable(
            PhoneContactsDestinations.ADD_EDIT_PHONE_CONTACT_ROUTE,
            arguments = listOf(
                navArgument(PHONE_CONTACT_ID_ARG) {
                    type = NavType.StringType; nullable = true
                }
            )
        ) { entry ->
            val viewModel: AddEditPhoneContactViewModel =
                viewModel(
                    factory = getPhoneContactsViewModelFactory(
                        appContainer = appContainer,
                        entry.arguments
                    )
                )
            val screenAttributes = AddEditPhoneContactScreenAttributes(
                onSave = { navActions.navigateToPhoneContacts() },
                onBack = { navController.popBackStack() }
            )

            AddEditPhoneContactScreen(
                viewModel = viewModel,
                screenAttrs = screenAttributes
            )
        }
        composable(PhoneContactsDestinations.STATISTICS_ROUTE) {
            val viewModel: StatisticsViewModel =
                viewModel(factory = getPhoneContactsViewModelFactory(appContainer = appContainer))
            AppModalDrawer(drawerState, currentRoute, navActions) {
                StatisticsScreen(
                    viewModel = viewModel,
                    openDrawer = { coroutineScope.launch { drawerState.open() } }
                )
            }
        }
    }
}

data class PhoneContactScreenAttributes(
    val onEditItem: (Int) -> Unit,
    val onDeleteItem: () -> Unit,
    val onBack: () -> Unit,
)

data class AddEditPhoneContactScreenAttributes(
    val onSave: () -> Unit,
    val onBack: () -> Unit,
)
