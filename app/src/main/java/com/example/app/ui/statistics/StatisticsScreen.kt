/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.app.ui.statistics

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.app.presentation.StatisticsUiState
import com.example.app.presentation.StatisticsViewModel
import com.example.app.ui.util.AnimatedCircle
import androidx.compose.ui.graphics.Color
import com.example.app.R
import com.example.app.ui.util.StatisticsTopAppBar

@Composable
fun StatisticsScreen(
    viewModel: StatisticsViewModel,
    openDrawer: () -> Unit,
    modifier: Modifier = Modifier,
    scaffoldState: ScaffoldState = rememberScaffoldState()
) {
    Scaffold(
        scaffoldState = scaffoldState,
        topBar = { StatisticsTopAppBar(R.string.app_name, openDrawer) },
        modifier = modifier.fillMaxSize()
    ) {
        val uiState by viewModel.uiState.collectAsState(initial = StatisticsUiState())

        StatisticsContent(
            totalItems = uiState.totalItems,
            totalFavoriteItems = uiState.totalFavoriteItems
        )
    }
}

@Composable
private fun StatisticsContent(
    totalItems: Int,
    totalFavoriteItems: Int,
    modifier: Modifier = Modifier
) {
    Column(modifier = modifier.verticalScroll(rememberScrollState())) {
        Box(Modifier.padding(16.dp)) {
            val items = listOf(totalFavoriteItems, totalItems - totalFavoriteItems)
            val listOfColors = listOf(
                Color.Green,
                Color.Yellow
            )
            val proportions = items.extractProportions { it.toFloat() }
            val circleColors = listOfColors.map { it }
            AnimatedCircle(
                proportions,
                circleColors,
                Modifier
                    .height(300.dp)
                    .align(Alignment.Center)
                    .fillMaxWidth()
            )
            Column(modifier = Modifier.align(Alignment.Center)) {
                Text(
                    text = totalItems.toString(),
                    style = MaterialTheme.typography.h2,
                    modifier = Modifier.align(Alignment.CenterHorizontally)
                )
                Text(
                    text = totalFavoriteItems.toString(),
                    style = MaterialTheme.typography.h2,
                    modifier = Modifier.align(Alignment.CenterHorizontally)
                )
            }
        }
    }
}

fun <E> List<E>.extractProportions(selector: (E) -> Float): List<Float> {
    val total = this.sumOf { selector(it).toDouble() }
    return this.map { (selector(it) / total).toFloat() }
}

/*data class StatisticsScreenAttrs(
    val viewModel: StatisticsViewModel
)*/
