package com.example.app.ui.addEditPhoneContact

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.example.app.R
import com.example.app.presentation.AddEditPhoneContactViewModel
import com.example.app.ui.AddEditPhoneContactScreenAttributes
import com.example.app.ui.util.AddEditPhoneContactTopAppBar
import com.example.app.ui.util.PhoneContactsAlertDialog
import java.util.*

@Composable
fun AddEditPhoneContactScreen(
    viewModel: AddEditPhoneContactViewModel,
    screenAttrs: AddEditPhoneContactScreenAttributes,
    modifier: Modifier = Modifier,
    scaffoldState: ScaffoldState = rememberScaffoldState()
) {
    val itemState = viewModel.getItem().collectAsState()
    val topBarTitle =
        if (itemState.value.isEditing) R.string.edit_phone_contact else R.string.add_phone_contact
    var showDialog by remember { mutableStateOf(false) }

    Scaffold(
        modifier = modifier.fillMaxSize(),
        scaffoldState = scaffoldState,
        topBar = {
            AddEditPhoneContactTopAppBar(
                topBarTitle,
                if (itemState.value.hasChanged && !showDialog) {
                    {
                        showDialog = true
                    }
                } else screenAttrs.onBack
            )
        },
        floatingActionButton = {
            FloatingActionButton(onClick = { viewModel.save() }) {
                Icon(Icons.Filled.Done, stringResource(id = R.string.save_phone_contact))
            }
        }
    ) { paddingValues ->
        BackHandler(itemState.value.hasChanged && !showDialog) {
            showDialog = true
        }

        val discardBodyText = stringResource(R.string.discard_message)
        if (showDialog) PhoneContactsAlertDialog(
            bodyText = discardBodyText,
            buttonText = stringResource(R.string.discard_button).uppercase(Locale.getDefault()),
            onClick = screenAttrs.onBack,
            onDismiss = { showDialog = false }
        )

        val contentAttrs = AddEditPhoneContactContentAttrs(
            name = itemState.value.name,
            phone = itemState.value.phone,
            isFavorite = itemState.value.isFavorite,
            onNameChanged = { viewModel.updateName(it) },
            onPhoneChanged = { viewModel.updatePhone(it) },
            onFavoriteChanged = { viewModel.updateFavoriteStatus(it) }
        )
        AddEditPhoneContactContent(
            contentAttrs = contentAttrs,
            modifier = Modifier.padding(paddingValues)
        )

        if (itemState.value.isSaved) {
            val snackBarText = stringResource(R.string.phone_contact_saved)
            LaunchedEffect(scaffoldState, snackBarText) {
                scaffoldState.snackbarHostState.showSnackbar(snackBarText)
                screenAttrs.onSave()
            }
        }
    }
}

@Composable
private fun AddEditPhoneContactContent(
    contentAttrs: AddEditPhoneContactContentAttrs,
    modifier: Modifier = Modifier
) {
    Column(
        modifier
            .fillMaxWidth()
            .padding(all = dimensionResource(id = R.dimen.horizontal_margin))
            .verticalScroll(rememberScrollState())
    ) {
        val textFieldColors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = Color.Transparent,
            unfocusedBorderColor = Color.Transparent,
            cursorColor = MaterialTheme.colors.secondary.copy(alpha = ContentAlpha.high)
        )
        OutlinedTextField(
            value = contentAttrs.name,
            modifier = Modifier.fillMaxWidth(),
            onValueChange = contentAttrs.onNameChanged,
            placeholder = {
                Text(
                    text = stringResource(id = R.string.name_hint),
                    style = MaterialTheme.typography.h6
                )
            },
            textStyle = MaterialTheme.typography.h6.copy(fontWeight = FontWeight.Bold),
            maxLines = 1,
            colors = textFieldColors
        )
        OutlinedTextField(
            value = contentAttrs.phone,
            onValueChange = contentAttrs.onPhoneChanged,
            placeholder = { Text(stringResource(id = R.string.phone_hint)) },
            modifier = Modifier
                .fillMaxWidth(),
            colors = textFieldColors
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = dimensionResource(id = R.dimen.list_item_padding))
        ) {
            Checkbox(
                checked = contentAttrs.isFavorite,
                onCheckedChange = { contentAttrs.onFavoriteChanged(it) }
            )
            Text(
                text = stringResource(id = R.string.is_favorite),
                style = MaterialTheme.typography.body1,
                modifier = Modifier.padding(
                    top = dimensionResource(id = R.dimen.vertical_checkout_text_margin),
                    start = dimensionResource(id = R.dimen.horizontal_margin)
                )
            )
        }
    }
}

data class AddEditPhoneContactContentAttrs(
    val name: String,
    val phone: String,
    val isFavorite: Boolean,
    val onNameChanged: (String) -> Unit,
    val onPhoneChanged: (String) -> Unit,
    val onFavoriteChanged: (Boolean) -> Unit
)
