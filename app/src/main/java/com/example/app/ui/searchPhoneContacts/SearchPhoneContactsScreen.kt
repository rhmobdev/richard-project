/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.app.ui.searchPhoneContacts

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.app.data.PhoneContact
import com.example.app.presentation.SearchPhoneContactsUiState
import com.example.app.presentation.SearchPhoneContactsViewModel
import com.example.app.ui.util.SearchPhoneContactsTopAppBar

@Composable
fun SearchPhoneContactsScreen(
    screenAttrs: SearchPhoneContactsScreenAttrs,
    modifier: Modifier = Modifier,
    scaffoldState: ScaffoldState = rememberScaffoldState()
) {
    val uiState =
        screenAttrs.viewModel.uiState.collectAsState(initial = SearchPhoneContactsUiState())

    Scaffold(
        scaffoldState = scaffoldState,
        modifier = modifier.fillMaxSize(),
        topBar = {
            SearchPhoneContactsTopAppBar(
                onBack = screenAttrs.onBack,
                searchTerm = uiState.value.searchTerm,
                updateSearchTerm = {
                    screenAttrs.viewModel.search(it)
                }
            )
        }
    ) { paddingValues ->
        SearchPhoneContactsContent(
            data = uiState.value.items,
            onItemClick = screenAttrs.onItemClick,
            modifier = Modifier.padding(paddingValues)
        )
    }
}

@Composable
private fun SearchPhoneContactsContent(
    data: List<PhoneContact>,
    onItemClick: (Int) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn {
        items(
            items = data,
            key = { it.name }
        ) { item ->
            Text(
                text = item.name,
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable(onClick = { onItemClick(item.id!!) })
                    .padding(
                        start = 16.dp,
                        top = 8.dp,
                        end = 16.dp,
                        bottom = 8.dp
                    )
                    .wrapContentWidth(Alignment.Start)
            )
        }
    }
}

data class SearchPhoneContactsScreenAttrs(
    val viewModel: SearchPhoneContactsViewModel,
    val onItemClick: (Int) -> Unit,
    val onBack: () -> Unit
)
