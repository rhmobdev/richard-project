package com.example.app.ui.util

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.app.R
import com.example.app.ui.theme.AndroidComposeAppTheme

@Composable
@Suppress("FunctionNaming")
fun PhoneContactsTopAppBar(@StringRes title: Int, startSearch: () -> Unit, openDrawer: () -> Unit) {
    TopAppBar(
        title = { Text(text = stringResource(title)) },
        navigationIcon = {
            IconButton(onClick = openDrawer) {
                Icon(Icons.Filled.Menu, stringResource(id = R.string.open_drawer))
            }
        },
        actions = {
            IconButton(onClick = startSearch) {
                Icon(Icons.Filled.Search, stringResource(id = R.string.start_search))
            }
        },
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
@Suppress("FunctionNaming")
fun SearchPhoneContactsTopAppBar(
    searchTerm: String,
    updateSearchTerm: (String) -> Unit,
    onBack: () -> Unit
) {
    TopAppBar(elevation = 0.dp) {
        IconButton(onClick = onBack) {
            Icon(Icons.Filled.ArrowBack, stringResource(id = R.string.menu_back))
        }
        BasicTextField(
            value = searchTerm,
            onValueChange = updateSearchTerm,
            textStyle = MaterialTheme.typography.subtitle1.copy(
                color = LocalContentColor.current
            ),
            maxLines = 1,
            cursorBrush = SolidColor(LocalContentColor.current),
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.CenterVertically)
        )
    }
}

@Composable
@Suppress("FunctionNaming")
fun PhoneContactTopAppBar(@StringRes title: Int, onBack: () -> Unit, onDelete: () -> Unit) {
    TopAppBar(
        title = {
            Text(text = stringResource(title))
        },
        navigationIcon = {
            IconButton(onClick = onBack) {
                Icon(Icons.Filled.ArrowBack, stringResource(id = R.string.menu_back))
            }
        },
        actions = {
            IconButton(onClick = onDelete) {
                Icon(Icons.Filled.Delete, stringResource(id = R.string.menu_delete_phone_contact))
            }
        },
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
@Suppress("FunctionNaming")
fun AddEditPhoneContactTopAppBar(@StringRes title: Int, onBack: () -> Unit) {
    TopAppBar(
        title = { Text(text = stringResource(title)) },
        navigationIcon = {
            IconButton(onClick = onBack) {
                Icon(Icons.Filled.ArrowBack, stringResource(id = R.string.menu_back))
            }
        },
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
@Suppress("UnusedPrivateMember", "FunctionNaming")
fun StatisticsTopAppBar(@StringRes title: Int, openDrawer: () -> Unit) {
    TopAppBar(
        title = { Text(text = stringResource(title)) },
        navigationIcon = {
            IconButton(onClick = openDrawer) {
                Icon(Icons.Filled.Menu, stringResource(id = R.string.open_drawer))
            }
        },
        modifier = Modifier.fillMaxWidth()
    )
}

@Preview
@Composable
@Suppress("UnusedPrivateMember", "FunctionNaming")
private fun PhoneContactsTopAppBarPreview() {
    AndroidComposeAppTheme {
        Surface {
            PhoneContactsTopAppBar(R.string.app_name, {}, {})
        }
    }
}

@Preview
@Composable
@Suppress("UnusedPrivateMember", "FunctionNaming")
private fun PhoneContactTopAppBarPreview() {
    AndroidComposeAppTheme {
        Surface {
            PhoneContactTopAppBar(R.string.phone_contact, { }, { })
        }
    }
}

@Preview
@Composable
@Suppress("UnusedPrivateMember", "FunctionNaming")
private fun AddPhoneContactTopAppBarPreview() {
    AndroidComposeAppTheme {
        Surface {
            AddEditPhoneContactTopAppBar(R.string.add_phone_contact) { }
        }
    }
}

@Preview
@Composable
@Suppress("UnusedPrivateMember", "FunctionNaming")
private fun EditPhoneContactTopAppBarPreview() {
    AndroidComposeAppTheme {
        Surface {
            AddEditPhoneContactTopAppBar(R.string.edit_phone_contact) { }
        }
    }
}
