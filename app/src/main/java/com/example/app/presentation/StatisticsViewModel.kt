package com.example.app.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app.domain.useCases.LoadPhoneContactsUseCase
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

data class StatisticsUiState(
    val totalItems: Int = 0,
    val totalFavoriteItems: Int = 0
) {
    val proportionFavoriteItems: Float
        get() {
            if (totalItems == 0) return 0f
            return totalFavoriteItems / totalItems.toFloat()
        }
}

class StatisticsViewModel(private val loadPhoneContactsUseCase: LoadPhoneContactsUseCase) :
    ViewModel() {
    private val _uiState = MutableStateFlow(StatisticsUiState())
    val uiState = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            loadPhoneContactsUseCase().collect { itemList ->
//                Log.d("error app", "error 1")
                _uiState.update {
                    it.copy(
                        totalItems = itemList.size,
                        totalFavoriteItems = itemList.filter { item -> item.isFavorite }.size
                    )
                }
            }
        }
    }
}
