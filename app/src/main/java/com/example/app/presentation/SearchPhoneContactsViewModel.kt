package com.example.app.presentation

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app.data.PhoneContact
import com.example.app.domain.useCases.LoadPhoneContactsUseCase
import kotlinx.coroutines.flow.*

const val SEARCH_TERM_SAVED_STATE_KEY = "SEARCH_TERM_SAVED_STATE_KEY"
const val SEARCH_TERM_SAVED_STATE_INITIAL_VALUE = ""

data class SearchPhoneContactsUiState(
    val items: List<PhoneContact> = emptyList(),
    val searchTerm: String = ""
)

class SearchPhoneContactsViewModel(
    private val loadPhoneContactsUseCase: LoadPhoneContactsUseCase,
    private val savedStateHandle: SavedStateHandle
) :
    ViewModel() {
    private val _savedSearchTerm =
        savedStateHandle.getStateFlow(
            SEARCH_TERM_SAVED_STATE_KEY,
            SEARCH_TERM_SAVED_STATE_INITIAL_VALUE
        )
    private val _filteredItems =
        combine(loadPhoneContactsUseCase(), _savedSearchTerm) { items, searchTerm ->
            items.filter {
                searchTerm.isEmpty() || it.name.contains(searchTerm) || it.phone.contains(searchTerm)
            }
        }
            .onStart { emit(emptyList()) }
    val uiState = combine(_filteredItems, _savedSearchTerm) { filteredItems, searchTerm ->
        SearchPhoneContactsUiState(items = filteredItems, searchTerm = searchTerm)
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(),
        initialValue = SearchPhoneContactsUiState()
    )

    fun search(text: String) {
        savedStateHandle[SEARCH_TERM_SAVED_STATE_KEY] = text
    }
}
