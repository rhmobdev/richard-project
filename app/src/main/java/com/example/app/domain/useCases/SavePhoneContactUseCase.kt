package com.example.app.domain.useCases

import com.example.app.data.PhoneContact
import com.example.app.data.PhoneContactMetadata
import com.example.app.domain.interfaces.PhoneContactsRepository
import java.text.DateFormat
import java.util.*

class SavePhoneContactUseCase(
    private val phoneContactsRepository: PhoneContactsRepository
) {
    suspend operator fun invoke(itemId: Int?, name: String, phone: String, isFavorite: Boolean) {
        val currentDateTime = DateFormat.getDateTimeInstance().format(Date())
        val metadata = PhoneContactMetadata(updateTime = currentDateTime)
        val item = PhoneContact(
            id = itemId,
            name = name,
            phone = phone,
            isFavorite = isFavorite,
            metadata = metadata
        )
        if (itemId == null) phoneContactsRepository.insert(item)
        else phoneContactsRepository.update(item)
    }
}
