# android-onboarding

Hola si estás viendo esta página por primera vez, lo más seguro es que necesites un poco de orientación

La idea de este documento es acelerar el proceso de onboarding de nuevos integrantes a la comunidad android de mobdev :)

Me imagino que si llegaste a clonar este repositorio, es por que ya tienes tu cuenta de gitlab creada con el correo de mobdev, por ejemplo name@mobdev.cl.

1. Lo primero que vamos hacer es crear una rama, para ir registrando tus avances.

```sh
git checkout master
git pull
git checkout -b onboarding/add-your-name-and-lastname-here
```

2. Una vez creada tu rama, podrás modificar este documento. A continuación veras unas lista de items, los cuales deberás ir completando, para terminar tu onboarding.

## Kotlin

Si vienes de Java y aún no tienes mucha experiencia, te recomendamos ver estos videos

- [x] [Kotlin for Java Develpers](https://es.coursera.org/learn/kotlin-for-java-developers)

    - [x] Week 1
    - [x] Week 2
    - [x] Week 3
    - [x] Week 4
    - [x] Week 5

- [x] Kotlin Coroutine
- [x] Kotlin Flow
- [x] Kotlin Infix

## Principios S.O.L.I.D

Por acá te recomendamos la siguiente charla

- [x] [SOLID en Kotlin con Memes](https://web.microsoftstream.com/embed/channel/886cc58a-a56a-44dc-8a43-4341322fcd8c?app=microsoftteams&sort=undefined&l=es-cl#)

## Test y inyección de dependencias

- [x] [Manual android de Car, Engine intro a inyección de dependencias y test](https://developer.android.com/training/dependency-injection?hl=es-419)

- [x] Taller de Car, Engine intro a inyección de dependencias y test

- [ ] Crear tu propio taller, por ejemplo con Casa ventana y puerta

## Clean Architecture y inversión de dependencias

- [x] Taller de clean e inversión de dependencias

## Dagger

- [ ] Taller de intro a Dagger

## Capa Data

- [x] Taller capa de data

## MVI

- [ ] Taller capa de presentación

## UI

- [x] Taller de UI

## JetPack Compose

- [x] [JetPack Compose Pathway](https://developer.android.com/courses/pathways/compose)
- [x] Taller JetPack Compose

## Gitflow

- [x] Git
- [x] Git flow
- [x] Git Rebase

## Scrum

- [x] [Documentación Scrum Atlassian](https://www.atlassian.com/agile/scrum)
- [x] Taller Ceremonias

## Proyectos internos

- [ ] Padok